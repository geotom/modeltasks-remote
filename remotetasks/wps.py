from remote import RemoteTask


class WPSTask(RemoteTask):
    """
    A class defining a remote tasks using the OGC WPS protocol
    """